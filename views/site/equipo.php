<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\GridView;

$this->title = 'My Yii Application';
?>
<div class="Equipos">
 <div class="body-content">
    <?=  var_dump($resultado) ?>
        <div class="row">
            <!-- boton consulta -->
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                       
                        <h3>Consulta 1</h3>
                        <p>Listar edades de ciclistas sin repetidos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/edadsinrepetidos'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class'=>'btn btn-warning'])?>
                        </p>
                        
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 2</h3>
                        <p>Listar edades de ciclistas sin repetidos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/edadsinrepetidos'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 3</h3>
                        <p>listar las edades de los ciclistas de Artiach</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta2'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta2a'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 4</h3>
                        <p>listar las edades de los ciclistas de Artiach o de Amore Vita</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta3'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta3a'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- boton consulta -->
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 5</h3>
                        <p>Listar edades de ciclistas sin repetidos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/edadsinrepetidos'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 6</h3>
                        <p>Listar edades de ciclistas sin repetidos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/edadsinrepetidos'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 7</h3>
                        <p>listar las edades de los ciclistas de Artiach</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta2'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta2a'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 8</h3>
                        <p>listar las edades de los ciclistas de Artiach o de Amore Vita</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta3'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta3a'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
     <div class="row">
            <!-- boton consulta -->
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 9</h3>
                        <p>Listar edades de ciclistas sin repetidos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/edadsinrepetidos'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 10</h3>
                        <p>Listar edades de ciclistas sin repetidos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/edadsinrepetidos'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 11</h3>
                        <p>listar las edades de los ciclistas de Artiach</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta2'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta2a'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 12</h3>
                        <p>listar las edades de los ciclistas de Artiach o de Amore Vita</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta3'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta3a'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
     <div class="row">
            <!-- boton consulta -->
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 13</h3>
                        <p>Listar edades de ciclistas sin repetidos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/edadsinrepetidos'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 14</h3>
                        <p>Listar edades de ciclistas sin repetidos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/edadsinrepetidos'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 15</h3>
                        <p>listar las edades de los ciclistas de Artiach</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta2'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta2a'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 16</h3>
                        <p>listar las edades de los ciclistas de Artiach o de Amore Vita</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta3'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta3a'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
     <div class="row">
            <!-- boton consulta -->
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 17</h3>
                        <p>Listar edades de ciclistas sin repetidos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/edadsinrepetidos'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 18</h3>
                        <p>Listar edades de ciclistas sin repetidos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/edadsinrepetidos'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 19</h3>
                        <p>listar las edades de los ciclistas de Artiach</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta2'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta2a'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 20</h3>
                        <p>listar las edades de los ciclistas de Artiach o de Amore Vita</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta3'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta3a'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
     <div class="row">
            <!-- boton consulta -->
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 21</h3>
                        <p>Listar edades de ciclistas sin repetidos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/edadsinrepetidos'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 22</h3>
                        <p>Listar edades de ciclistas sin repetidos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/edadsinrepetidos'], ['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
    </div>
