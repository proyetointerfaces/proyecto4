<?php
use yii\bootstrap4\Html;
?>

<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
        <?= Html::img('@web/img/1.jpg',['class' => 'd-block w-100' ], ['alt' => 'My logo']) ?>
    </div>
    <div class="carousel-item">
        <?= Html::img('@web/img/2.jpg',['class' => 'd-block w-100' ], ['alt' => 'My logo']) ?>
    </div>
    <div class="carousel-item">
        <?= Html::img('@web/img/3.jpg',['class' => 'd-block w-100' ], ['alt' => 'My logo']) ?>
    </div>
  </div>
</div>



<div class="card text-white bg-info mb-3 mt-5">
  <div class="card-header">Header</div>
  <div class="card-body">
    <h5 class="card-title">Info card title</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  </div>
</div>
